let canvas = document.getElementById('myCanvas');
let ctx = canvas.getContext('2d');

let x = canvas.width / 2; // initial x position
let y = canvas.height - 30; // initial y position
let dx = 2; // change in x
let dy = -2; // change in y
let ballRadius = 10; // radius of ball

// function to draw the ball
function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
    ctx.fillStyle = '#0095DD';
    ctx.fill();
    ctx.closePath();
}

// function to animate the ball
function animateBall() {
    ctx.clearRect(0, 0, canvas.width, canvas.height); // clear canvas
    drawBall(); // draw the ball
    x += dx; // change x
    y += dy; // change y

    // boundary detection for right/left side
    if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
        dx = -dx; // reverse direction
    }

    // boundary detection for top/bottom
    if (y + dy > canvas.height - ballRadius || y + dy < ballRadius) {
        dy = -dy; // reverse direction
    }
}

setInterval(animateBall, 10); // animate every 10 milliseconds
